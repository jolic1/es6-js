/*

作業一
Math.random()方法是在JavaScript中產生亂數的一個方法。不論你要作野球拳遊戲，還是線上博杯拿大獎的應用程式，都可以用它來產生亂數。
Math.random會隨時產生一個0到1的浮點數，它的基本用法如下:

Math.floor(Math.random() * 6) + 1
上面的語句代表要產生1到6的整數亂數
1代表開始數字
6代表可能情況有6種
所以如果是以最大值max，最小值min來寫這個語句，這也是產生整數的亂數，會像下面這樣:

Math.floor(Math.random() * (max - min + 1)) + min
現在要利用這個產生亂數的方法，作一個線上抽獎的活動，客戶說有下面幾個獎品:

50吋液晶電視 1台
PS4遊戲機 3台
手機行動充電器 10台
7-11的100元購物券 100張
預計參與抽獎的人數有1萬人，要如何來寫這個程式的抽獎過程的應用程式？

*/

const lotteryMaxPeople = 10000
const lotteryMinPeople = 1

function getBingoNo(lotteryMaxPeople,lotteryMinPeople){
  let bingoNo = Math.floor(Math.random() * (lotteryMaxPeople - lotteryMinPeople + 1)) + lotteryMinPeople
  return bingoNo
}
// 抽出50吋液晶電視 1台
console.log('50吋液晶電視 1台，中獎人的編號是：')
console.log(getBingoNo(lotteryMaxPeople,lotteryMinPeople))

// PS4遊戲機 3台
console.log('PS4遊戲機 3台，中獎人的編號是：')
for (let i = 0; i < 3; i++){
  console.log(getBingoNo(lotteryMaxPeople,lotteryMinPeople))
}

// 手機行動充電器 10台
console.log('手機行動充電器 10台，中獎人的編號是：')
for (let i = 0; i < 10; i++){
  console.log(getBingoNo(lotteryMaxPeople,lotteryMinPeople))
}

// 7-11的100元購物券 100張
console.log('7-11的100元購物券 100張，中獎人的編號是：')
for (let i = 0; i < 100; i++){
  console.log(getBingoNo(lotteryMaxPeople,lotteryMinPeople))
}

/*

作業二
有一家日本的房地產公司來台灣設立新的分公司，因為日本的房地產用的計算單位與台灣的不同，
所以要委託你寫一個程式，這個程式是希望能轉換平方公尺為坪數。要如何來寫這個轉換的公式？

*/

let squareMeters = 3
const ping = squareMeters * 0.3025
console.log(squareMeters + '平方公尺' + ' = ' + ping + '坪')