/*

作業一
公司交給你一個案子，要撰寫一支對網站上線上填寫表單進行檢查欄位的程式，以下是這個表單的欄位與要檢查的規則:

姓名(fullname): 最多4個字元，必填
手機號碼(mobile): 手機號碼必需是10個數字
出生年月日(birthday): 年1970-2000，月份1-12，日期1-31
住址(address): 最少8個字元，最多50個字元，必填
Email(email): 最少10個字元，最多50個字元，必填
請問要如何寫出每個欄位的判斷檢查的程式碼。

*/

const fullname = 'Tony'
const mobile = '0912345678'
const birthday = '19960230'
const address = '台中市西屯區xxxx'
const email = 'meddd@cici.com.tw'

const birthYear = birthday.slice(0,4)
const birthMonth = birthday.slice(4,6)
const birthDay = birthday.slice(6,8)

switch (true) {
    case (fullname.length < 4 || fullname === 0):
        console.log('姓名最多4個字元且為必填')
    case (mobile.length !== 10):
        console.log('手機號碼必須是10個數字')
    case (birthYear <= 1970 || birthYear >= 2000 ||  birthMonth < 1 || birthMonth > 12 || birthDay < 1 || birthDay > 31):
        console.log('生日的年份必須是1970-2000，月份1-12，日期1-31')
    case (address.length < 8 || address.length > 50 || address.length === 0):
        console.log('住址最少8個字元，最多50個字元且為必填')
    case (email.length < 10 || email.length > 50 || email.length === 0):
        console.log('E-mail最少10個字元，最多50個字元且為必填')
        break;
    default:
        console.log('資料填寫正確並無誤')
        break;
}
